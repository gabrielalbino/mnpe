#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double x){
	return x; //coloque a função aqui
}

int main(int argc, char** argv){
	double eps = 1e-4;
	if(argc < 3){
		printf("Você deve especificar dois valores iniciais\n");
		return -1;
	}
	float x0 = atof(argv[1]);
	float x1 = atof(argv[2]);
	float xExt = x1 - ((x1-x0)/f(x1)-f(x0))*f(x1);
	while(fabs(xExt-x1) > eps){
		x0 = x1;
		x1 = xExt;
		xExt = x1 - ((x1-x0)/f(x1)-f(x0))*f(x1);
	}
	printf("Raiz: %17.15f\n", xExt);

	return 0;
}
