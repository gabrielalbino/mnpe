#include <math.h>
#include <stdio.h>
int main(){
	float Tk, T0 = 1/sqrt(3.f);
	int i;
	for(i = 0; i < 28; i++){
			Tk = (sqrt(T0*T0+1)-1)/T0;
			printf("%3d %25.23f\n", i, 4*6*pow(2,i)*(sqrt(Tk*Tk+1)-1)/Tk);
			T0=Tk;
	}
	return 0;
}
