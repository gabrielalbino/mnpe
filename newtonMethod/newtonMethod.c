#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double x){
	return x*x-9.0;
}

int main(int argc, char** argv){
	double xk0 = atof(argv[1]);
	double xk1;
	double eps = 1e-4;
	int contador = 1;
	xk1 = xk0 - f(xk0)/(2*xk0);
	while(fabs(xk1-xk0)>eps){
		xk0 = xk1;
		xk1 = xk0 - f(xk0)/(2*xk0);
		contador++;
	}
	printf("Solução: %17.15f", xk1);
}
