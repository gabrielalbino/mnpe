/*Aluno: Gabriel Batista Albino Silva			Matricula: 160028361*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*Sintese:
 *Objetivo: Converter sqrt(2) para binário;
 *Entrada: Nada
 *Saída: sqrt(2) convertido para binário
*/

#define UMACASA 1.4
#define DUASCASAS 1.41
#define TRESCASAS 1.414
#define QUATROCASAS 1.4142
#define CINCOCASAS 1.41421
#define SEISCASAS 1.414214

int main(){
	/*Declaração de funções*/
	void intToBinary(float num, char*binario, int mostrarErro);
	/*Declaração de variaveis*/
	char binario[129];
	/*Instuções*/
	intToBinary(UMACASA, binario, 0);
	printf("%f - %s\n", UMACASA, binario);
	intToBinary(DUASCASAS, binario, 0);
	printf("%f - %s\n",DUASCASAS, binario);
	intToBinary(TRESCASAS, binario, 0);
	printf("%f - %s\n",TRESCASAS, binario);
	intToBinary(QUATROCASAS, binario, 0);
	printf("%f - %s\n",QUATROCASAS, binario);
	intToBinary(CINCOCASAS, binario, 0);
	printf("%f - %s\n",CINCOCASAS, binario);
	intToBinary(SEISCASAS, binario, 0);
	printf("%f - %s\n",SEISCASAS, binario);
	printf("\n\nObsevação: O numero binário correspondente a 0.4 sofre erro de conversão binária, pois mostrando-o após cada iteração do programa temos:\n");
	intToBinary(UMACASA, binario, 1);
	printf("E isso influênciou no resultado do número binário!\n\n\n");

}

void intToBinary(float num, char*binario, int mostrarErro){
	/*Declaração de funções*/
	/*Declaração de variaveis*/
	int firstRun = 1, pos = 0;
	/*Instuções*/
	do{
		if(mostrarErro){
			printf("%f\n", num);
		}
		if(firstRun != 0){
			if(num >= 1 && num < 2){
				binario[pos++] = '1';
				binario[pos++] = '.';
			}
			else if(num < 1){
				binario[pos++] = '0';
				binario[pos++] = '.';
			}
			num= num-1;
			firstRun = 0;
		}
		else{
			num *= 2;
			if(num >= 1){
				binario[pos++] = '1';
				num -= 1;
			}
			else if(num < 1){
				binario[pos++] = '0';
			}
		}
		binario[pos+1] = '\0';
	}while(num != 0.f && pos < 129);

}
