# Trabalho T0

1)Faça um programa para converter para binário:

sqrt 2 = 1,4

sqrt 2 = 1,41

sqrt 2 = 1,414

sqrt 2 = 1,4142

sqrt 2 = 1,41421

sqrt 2 = 1,414214

2) Os nrs binários terão um padrão de repetição. Se o periodo for a quantidade de bits do padrão de repetição, conforme a precisão da raiz aumenta como aumenta o periodo do padrão de repetição do nr na representação binária?

3) Seja o programa:

#include <stdio.h>
int main(){
    float x = 0.1;   
    printf("%17.15f\n\n",x);
    return 0;
}
irá imprimir:
0.100000001490116

Converta o nr 0.1 para binário padrão IEEE 754, ou seja, s=0, expoente = -4 (binário complemento 2), mais 23 bits ... cuidado com o arredondamento. Faça a conversão deste nr para decimal de formas a obter o mesmo que o computador imprimiu pelo prog C acima.

para converter:

1.b1b2...b23 = (1+ b1*2^(-1)+b1*2^(-2)+...+b23*2^(-23) )2^(-4)

e b1, b2, .... são os bits da conversão de 0.1 para binário.
