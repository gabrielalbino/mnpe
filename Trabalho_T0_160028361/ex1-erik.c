/* Aluno: Erik Busnello Imbuzeiro
Matricula: 15/0124325 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double d1 = 1.4, d2 = 1.41, d3 = 1.414, d4 = 1.4142, d5 = 1.41421, d6 = 1.414214;

void makebinary (double decimal, char*binary) {
	int first = 1, position = 0;
	
	while (decimal != 0 && position < 100) {
		if (first != 0) {
			if (decimal >= 1 && decimal < 2) {
				binary[position++] = '1';
				binary[position++] = '.';
			}
			else if (decimal < 1) {
				binary[position++] = '0';
				binary[position++] = '.';
			}
			decimal -= 1;
			first = 0;
		} else {
			decimal *= 2;
			if (decimal >= 1) {
				binary[position++] = '1';
				decimal -= 1;
			}
			else if (decimal < 1) {
				binary[position++] = '0';
			}
		}
		binary[position + 1] = '\0';
	}
}

int main () {
	
	void makebinary (double decimal, char*binary);
	char binary[100];
	
	makebinary (d1, binary);
	printf ("%lf - %s\n", d1, binary); 
	
	makebinary (d2, binary);
	printf ("%lf - %s\n", d2, binary); 
	
	makebinary (d3, binary);
	printf ("%lf - %s\n", d3, binary); 
	
	makebinary (d4, binary);
	printf ("%lf - %s\n", d4, binary); 
	
	makebinary (d5, binary);
	printf ("%lf - %s\n", d5, binary); 
	
	makebinary (d6, binary);
	printf ("%lf - %s\n", d6, binary); 
	
	return 0;
}

