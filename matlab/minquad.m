x = -3:2.25:21.75;
x = x'; 
func = 'x.^3+3.*x.^2+x+5';
y = eval(func);
y = y';

g0 = [ones(size(x))]';
g1 = [x]';
g2 = [x.^2]';
g3 = [x.^3]';

M = [dot(g0,g0) dot(g0,g1) dot(g0,g2) dot(g0,g3);
dot(g1,g0) dot(g1,g1) dot(g1,g2) dot(g1,g3);
dot(g2,g0) dot(g2,g1) dot(g2,g2) dot(g2,g3);
dot(g3,g0) dot(g3,g1) dot(g3,g2) dot(g3,g3);];

b = [dot(g0,y) dot(g1,y) dot(g2,y) dot(g3,y)]';

c = M\b;

z = c(1)+(c(2)*x)+(c(3)*x.^2)+(c(4)*x.^3);

plot(x,y,'o',x,z,'x');

