#include <stdlib.h>
#include <stdio.h>
#define NUM_COMMANDS 4

int main()
{
    char * commandsForGnuplot[] = {"set xrange[0:2]", "plot sin(1/x)/x", "replot tan(x)/x" , "replot sin(x)/x"};
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    int i;
    for (i=0; i < NUM_COMMANDS; i++)
    {
 	fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }
    return 0;
}
