#include <stdio.h>
#include <stdlib.h>

void mostraGrafico(){
	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
	char * commandsForGnuplot[] = {"set title \"Gráfico\"", "set xlabel \"x\"", "set ylabel\"f(x)\"", "set xrange[0:1]", "set yrange[0:4]", "plot 4/(x+1)"};
	int x;
	for(x = 0; x < 6; x++){
		fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[x]);
	}
	return;
}

double f(double x){
	return 4.f/(1.f+x);
}

double monteCarlo(int pontos){
	int n;
	double x, y, soma = 0, area = 4;
	for(n = 0; n < pontos; n++){
		x = rand() % 10001; //gera um valor entre 0 e 10000
		x /= 10000.f; //transforma ele em um valor entre 0 e 1;
		y = rand() % 40001; //gera um valor entre 0 e 40000
		y /= 10000.f; //transfora ele em um valor entre 0 e 4;
		if(y <= f(x)){
			soma++;
		}
	}
	return area*soma/pontos;
}

double trapezio(int intervalos){
	double deltaXk = 1.f/intervalos;
	int i;
	double soma = 0, xk = deltaXk;
	for(i= 1;i < intervalos; i++){
		soma += f(xk);
		xk += deltaXk;
	}
	return (deltaXk/2)*(2*soma + f(0) + f(1));
}

int main(){
	int n;
	mostraGrafico();
	printf("Insira o método desejado:\n1 - Método de Monte Carlo\n2 - Método dos trapézios\n");
	scanf("%d", &n);
	switch(n){
		case 1:
			printf("Insira o numero de pontos: ");
			scanf("%d", &n);
			printf("Integral com %d pontos = %15.17lf", n, monteCarlo(n));
			break;
		case 2:
			printf("Insira o numero de intervalos: ");
			scanf("%d", &n);
			printf("Integral com %d intervalos = %15.17lf\n", n, trapezio(n));
	}
	return 0;
}
