#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define H 0.1

double f(double x){
	return exp(-2*x);
}

double retangulo(double lsup){
	double xk0 =0, xk1 =H;
	float soma = 0;
	while(xk1 <= lsup){
		soma+=f((xk1+xk0)/2);
		xk0+=H;
		xk1+=H;
	}
	return soma*H;
}

double trapezio(double lsup){
	double soma = 0, xk = H;
	while(xk < lsup){
		soma += f(xk);
		xk += H;
	}

	return (H/2)*(2*soma + f(0) + f(lsup));
}

double sympson(double lsup){
	double soma = 0, xk = H;
	while(xk < lsup){
		soma+=f(xk);
		xk += H;
	}
	return ((H/3.f)*(f(0)+4*soma+f(lsup)));
}

void plot(double x1){
	FILE* handle = fopen("dados.dat", "w+");
	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
	double i = 0;
	while(i <= x1){
		fprintf(handle, "%lf %lf %lf %lf \n",i ,retangulo(i), trapezio(i), sympson(i));
		i+=H;
	}
	char * commandsForGnuplot[] = {"plot 'dados.dat' using 1:2 title 'retangulo'" , "replot 'dados.dat' using 1:3 title 'trapezio'", "replot 'dados.dat' using 1:4 title 'sympson'"};
	int j;
	for(j = 0; j < 3; j++){
		fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[j]);
	}
}

int main(){
	int n;
	double x1;
	printf("Escolha uma opção:\n");
	printf("1 - Método do retângulo composto\n");
	printf("2 - Método do trapezio composto\n");
	printf("3 - Método de sympson\n");
	printf("4 - plotar gráficos\n");
	scanf("%d", &n);
	printf("insira o valor de x: ");
	scanf("%lf", &x1);
	switch(n){
		case 1:
			printf("Integral pelo método do retângulo: %15.17lf\n", retangulo(x1));
			break;
		case 2:
			printf("Integral pelo método do trapézio: %15.17lf\n", trapezio(x1));
			break;
		case 3:
			printf("Integral pelo método de sympson: %15.17lf\n", sympson(x1));
			break;
		case 4:
			plot(x1);
	}
	return 0;
}
